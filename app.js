const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());
const dotenv = require('dotenv');
dotenv.config({ path: '.env' });
const port = process.env.PORT;
const logsPath = './logs/';
const { getConfig } = require('./logs/appLog');
const logger = getConfig(logsPath);

//Importez les fonctions
const  eligibleSms = require('./services/CronEligible');
const  LancementSms  = require('./services/cronLancement');
const  nonEligibleSms  = require('./services/CronNonEligible'); 
const  Nontop500Sms  = require('./services/CronNonTop500');
const  top500Sms  = require('./services/CronTop500'); 
const  beneficiarySms  = require('./services/CronNewBeneficiary'); 
const main2  = require('./controllers/info_pdv_journalier');
const main1 = require('./controllers/info_pdv_mensuelle');
const update_toppdv = require('./update/update_toppdv');
const update_top500 = require('./update/update_top500');

//Créez l'API pour la fonction eligibleSms
app.get('/api/eligibleSms', async (req, res) => {
    try {
        await eligibleSms();
        res.json({ message: 'Lancement de l\'envoi de SMS éligibles.' });
    } catch (error) {
        res.status(500).json({ message: 'Erreur lors du lancement de l\'envoi de SMS éligibles.', error: error.message });
    }
});

app.get('/api/lancementSms', async (req, res) => {
    try {
        await LancementSms();
        res.json({ message: 'Lancement de l\'envoi de SMS de lancement.' });
    } catch (error) {
        res.status(500).json({ message: 'Erreur lors du lancement de l\'envoi de SMS de lancement.', error: error.message });
    }
});

app.get('/api/beneficiarySms', async (req, res) => {
    try {
        await beneficiarySms();
        res.json({ message: 'Lancement de l\'envoi de SMS pour les nouveaux bénéficiaires.' });
    } catch (error) {
        res.status(500).json({ message: 'Erreur lors du lancement de l\'envoi de SMS pour les nouveaux bénéficiaires.', error: error.message });
    }
});


app.get('/api/nontop500Sms', async (req, res) => {
    try {
        await Nontop500Sms();
        res.json({ message: 'Lancement de l\'envoi de SMS pour les non top 500.' });
    } catch (error) {
        res.status(500).json({ message: 'Erreur lors du lancement de l\'envoi de SMS pour les non top 500.', error: error.message });
    }
});



app.get('/api/top500Sms', async (req, res) => {
    try {
        await top500Sms();
        res.json({ message: 'Lancement de l\'envoi de SMS pour les top 500.' });
    } catch (error) {
        res.status(500).json({ message: 'Erreur lors du lancement de l\'envoi de SMS pour les top 500.', error: error.message });
    }
});

app.get('/api/nonEligibleSms', async (req, res) => {
    try {
        await nonEligibleSms();
        res.json({ message: 'Lancement de l\'envoi de SMS pour les non éligibles.' });
    } catch (error) {
        res.status(500).json({ message: 'Erreur lors du lancement de l\'envoi de SMS pour les non éligibles.', error: error.message });
    }
});


app.get('/api/infopdvjournalier', async (req, res) => {
    try {
        await main2();
        res.json({ message: 'l\'insertion dans la table journaliere a abouti.' });
    } catch (error) {
        res.status(500).json({ message: 'Erreur lors du lancement de l\'envoi de SMS pour les nouveaux bénéficiaires.', error: error.message });
    }
});

app.get('/api/infopdvmensuelle', async (req, res) => {
    try {
        await main1();
        res.json({ message: ' l\'insertion dans la table mensuelle a abouti.' });
    } catch (error) {
        res.status(500).json({ message: 'Erreur lors du lancement de l\'envoi de SMS pour les nouveaux bénéficiaires.', error: error.message });
    }
});

app.get('/api/updateTop500', async (req, res) => {
    try {
        await update_top500();
        res.json({ message: ' Mise a jour du top500.' });
    } catch (error) {
        res.status(500).json({ message: 'Erreur lors de la mise a jour du top500', error: error.message });
    }
});

app.get('/api/updateToppdv', async (req, res) => {
    try {
        await update_toppdv();
        logger.info({ message: ' Mise a jour du toppdv.' });
    } catch (error) {
        res.status(500).json({ message: 'Erreur lors de la mise a jour du toppdv', error: error.message });
    }
});

// Écoutez sur le port 
app.listen(port, () => {
    console.log(`Serveur écoutant sur le port ${port}`);
});


