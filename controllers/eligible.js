const {dbConnection} = require('../models/database')
const { Client, Pool } = require("pg");
const dotenv = require('dotenv');
dotenv.config({ path: '../.env' });
const logsPath = '../logs/';
const { getConfig } = require('../logs/appLog');
const logger = getConfig(logsPath);

const pool = new Pool(dbConnection);

async function eligible() {
    try {
        const client = await pool.connect();

const createViewQuery = `
        
                INSERT INTO eligible
                SELECT reg1 , ms1 ,caa
                FROM (
                    SELECT *,
                    ROW_NUMBER() OVER (PARTITION BY reg1 ORDER BY reg1) AS rn
                    FROM toppdv
                    WHERE reg1 IN ('LOME', 'KARA', 'CENTRALE', 'PLATEAU', 'SAVANE') AND ms1 not in (select ms1 from non_eligible)
                ) AS ranked_data
                WHERE rn <= 100;
        `;

    await client.query(createViewQuery);
    logger.info('La table "eligible" a été populée avec succès.');
    client.release();
    
    } catch (error) {
        logger.error('Erreur lors de la population de la table eligible :', error);
        throw error;
    }
}

module.exports = eligible;

