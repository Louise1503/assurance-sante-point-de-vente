const parse = require('csv-parse');

const csvtojson = require('csvtojson')

const dotenv = require('dotenv');
dotenv.config({ path: '../.env' });
const { Client, Pool } = require("pg");
const logsPath = '../logs/';
const { getConfig } = require('../logs/appLog');
const logger = getConfig(logsPath);


const dbConnection = {
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    schema: process.env.DB_SCHEMA
}



const saveItems = async (allSQLInOne) => {
    console.log(allSQLInOne.slice(0, -1))

    const query1 = `INSERT INTO info_pdv_journalier (date, msisdn, grs, region, sell_out_moy, sell_out, objectif, sell_out_jours, sell_out_cumul, taux_mois)
    SELECT  TO_DATE(data.date, 'YYYY-MM-DD'), data.msisdn, data.grs, data.region, data.sell_out_moy, data.sell_out, data.objectif, data.sell_out_jours, data.sell_out_cumul, data.taux_mois FROM             
    (SELECT * FROM (VALUES ${allSQLInOne.slice(0, -1)})AS data2 (date, msisdn, grs, region, sell_out_moy, sell_out, objectif, sell_out_jours, sell_out_cumul, taux_mois)) AS data
WHERE NOT EXISTS (SELECT 1 FROM info_pdv_journalier AS i WHERE i.date::text = data.date AND i.msisdn = data.msisdn);
`;


    const client = new Client({ ...dbConnection });
    try {
        await client.connect()

        await client.query(query1)
    } catch (error) {
        console.log(error)
    } finally {
        await client.end();
    }
}

const stringFormat = (item) => {
    return (item === null || !item) ? null : ("'" + item + "'");
};

const formatJsonToSql = (fileData) => {
    let allSQLInOne = ''

    for (let i = 0; i < fileData.length; i++) {
        const item = fileData[i];
        allSQLInOne += `(${stringFormat(item.date)},${stringFormat(item.msisdn)},${stringFormat(item.grs)},${stringFormat(item.region)},${item.sell_out_moy},${item.sell_out},${item.objectif},${item.sell_out_jours},${item.sell_out_cumul},${item.taux_mois}),`
    }

    return allSQLInOne
}

const readFiles = async () => {
    const filePath = process.env.FILEPATH1;

    let fileData = []

    try {
        fileData = await csvtojson({
            trim: true,
        }).fromFile(filePath)
    } catch (err) {
        console.error('Erreur lors de la lecture du fichier :', err);
        fileData = []
    }

    return fileData
}

async function main2()  {
try {

    const fileData = await readFiles()
    let dataToInsert = formatJsonToSql(fileData)
    await saveItems(dataToInsert)
    logger.info('les données ont éte bien insseré dans la table info_pdv_journalier');
} catch (error) {
    logger.error('erreurs :',error);
}
}
main2(),
module.exports =  main2 ;