const parse = require('csv-parse');

const csvtojson = require('csvtojson')

const dotenv = require('dotenv');
dotenv.config({ path: '../.env' });
const logsPath = '../logs/';
const { getConfig } = require('../logs/appLog');
const logger = getConfig(logsPath);

const { Client, Pool } = require("pg");
const dbConnection = {
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    schema: process.env.DB_SCHEMA
}

const saveItems = async (allSQLInOne) => {
    console.log(allSQLInOne.slice(0, -1))

    const query1 = `INSERT INTO info_pdv_mensuelle (date, msisdn, objectifs_mois, sell_out_mois, taux_mois, mois)
    SELECT  TO_DATE(data.date, 'YYYY/MM/DD'),data.msisdn, data.objectifs_mois::integer, data.sell_out_mois, data.taux_mois,data.mois FROM               
    (SELECT * FROM (VALUES ${allSQLInOne.slice(0, -1)})AS data2 (date, mois, msisdn, objectifs_mois, sell_out_mois, taux_mois)) AS data
WHERE NOT EXISTS (SELECT 1 FROM info_pdv_mensuelle AS i WHERE i.mois = data.mois AND i.msisdn = data.msisdn);
`;

    const client = new Client({ ...dbConnection });
    try {
        await client.connect()
        await client.query(query1)
    } catch (error) {
        console.log(error)
    } finally {
        await client.end();
    }
}

const stringFormat = (item) => {
    return (item === null || !item) ? null : ("'" + item + "'");
};

const formatJsonToSql = (fileData) => {
    let allSQLInOne = ''

    for (let i = 0; i < fileData.length; i++) {
        const item = fileData[i];
        allSQLInOne += `(${stringFormat(item.date)},${stringFormat(item.mois)},${stringFormat(item.msisdn)},${stringFormat(item.objectifs_mois)},${item.sell_out_mois},${item.taux_mois}),`
    }

    return allSQLInOne
}

const readFiles = async () => {
    const filePath = process.env.FILEPATH2;

    let fileData = []

    try {
        fileData = await csvtojson({
            trim: true,
        }).fromFile(filePath)
    } catch (err) {
        console.error('Erreur lors de la lecture du fichier :', err);
        fileData = []
    }

    return fileData
}


async function main1() {

    const fileData = await readFiles()
    let dataToInsert = formatJsonToSql(fileData)
    await saveItems(dataToInsert)
    logger.info('les données ont éte bien inseré dans la table info_pdv_mensuelle')

}



module.exports =  main1 ;