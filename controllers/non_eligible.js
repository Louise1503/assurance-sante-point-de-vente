const { Client, Pool } = require("pg");
const dotenv = require('dotenv');
dotenv.config({ path: '../.env' });
const logsPath = '../logs/';
const { getConfig } = require('../logs/appLog');
const logger = getConfig(logsPath);

const dbConnection = {
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    schema: process.env.DB_SCHEMA
}
const pool = new Pool(dbConnection);

async function non_eligible() {
    try {
        const client = await pool.connect();

        // Vérifier si la table existe déjà
        const checkTableQuery = "SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name = 'non_eligible')";
        const checkTableResult = await client.query(checkTableQuery);
        const viewExists = checkTableResult.rows[0].exists;

        if (viewExists) {
            // Supprimer la table existante avant de la recréer

const createViewQuery = `
                    INSERT INTO non_eligible 
            WITH totals AS (
                SELECT
                    msisdn,
                    SUM(objectifs_mois) AS total_objectifs,
                    SUM(sell_out_mois) AS total_sell_out
                FROM
                    info_pdv_mensuelle
                WHERE
                    date >= DATE_TRUNC('month', NOW()) - INTERVAL '6 months'
                GROUP BY
                    msisdn
            )
            SELECT
                p.ms1,
                p.reg1,
                total_objectifs,
                total_sell_out
            FROM
                sauvegarde p
            INNER JOIN
                totals t ON p.ms1 = t.msisdn
            WHERE
                t.total_sell_out< t.total_objectifs;
        `;

        await client.query(createViewQuery);
        logger.info('La table "non_eligible" a été populée avec succès.');
        } else {
            logger.error('La table "top500" n\'existe pas.');
        }
        client.release();
    }

        catch (error) {
        logger.error('Erreur lors de linsertion de la table :', error);
        throw error;
    }
}

module.exports = non_eligible;