const { Client, Pool } = require("pg");
const dotenv = require('dotenv');
dotenv.config({ path: '../.env' });
const logsPath = '../logs/';
const { getConfig } = require('../logs/appLog');
const logger = getConfig(logsPath);

const dbConnection = {
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    schema: process.env.DB_SCHEMA
}
const pool = new Pool(dbConnection);

async function top500() {
    try {
        const client = await pool.connect();

        // Vérifier si la table existe déjà
        const checkTableQuery = "SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name = 'top500')";
        const checkTableResult = await client.query(checkTableQuery);
        const viewExists = checkTableResult.rows[0].exists;

        if (viewExists) {
            // Supprimer la table existante avant de la recréer
            // const dropViewQuery = "DROP VIEW table_top500";
            // await client.query(dropViewQuery);
            // console.log('La table "table_top500" existante a été supprimée.');
        

        const createViewQuery = `
        INSERT INTO top500 
    SELECT reg1 , ms1 ,caa
    FROM (
        SELECT *,
        ROW_NUMBER() OVER (PARTITION BY reg1 ORDER BY reg1) AS rn
        FROM toppdv
        WHERE reg1 IN ('LOME', 'KARA', 'PLATEAU', 'CENTRALE', 'SAVANE')
    ) AS ranked_data
    WHERE rn <= 100;

        `;
        
        await client.query(createViewQuery);
            logger.info('La table "top500" a été populée avec succès.');
        } else {
            console.log('La table "top500" n\'existe pas.');
        }

        client.release();
    }
    catch (error) {
        logger.error('Erreur lors de la population de la table :', error);
        throw error;
    }
}

module.exports = top500;