const dotenv = require('dotenv');
dotenv.config({ path: '../.env' });

const { Client, Pool } = require("pg");
const logsPath = '../logs/';
const { getConfig } = require('../logs/appLog');
const logger = getConfig(logsPath);

const dbConnection = {
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    schema: process.env.DB_SCHEMA
}
const pool = new Pool(dbConnection);
async function toppdv() {
    try {
        const client = await pool.connect();

        // Vérifier si la table existe déjà
        const checkTableQuery = "SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name = 'toppdv')";
        const checkTableResult = await client.query(checkTableQuery);
        const viewExists = checkTableResult.rows[0].exists;

        if (viewExists) {
            // Créer la table uniquement si elle n'existe pas déjà
const createTableQuery = `
                    INSERT INTO toppdv 
                WITH info_pdv_mensuelle AS (
                SELECT DISTINCT ON(msisdn)
                        msisdn,
                        mois,
                        SUM(sell_out_mois) AS ca
                    FROM
                        info_pdv_mensuelle
                    WHERE
                        date >= DATE_TRUNC('year', NOW()) - INTERVAL '1 year' 
                    GROUP BY
                        msisdn, mois
                )

                SELECT DISTINCT ON(j.msisdn)
                    j.region as reg1,
                    j.msisdn as ms1,
                    m.ca AS caa
                FROM
                    info_pdv_journalier j
                INNER JOIN
                    info_pdv_mensuelle m ON j.msisdn = m.msisdn AND TO_CHAR(j.date, 'YYYY-MM') = m.mois
                    where region = 'LOME'

                    
                    union all
                    SELECT DISTINCT ON(j1.msisdn)
                    j1.region as reg1,
                    j1.msisdn as ms1,
                    m.ca AS caa
                FROM
                    info_pdv_journalier j1
                INNER JOIN
                    info_pdv_mensuelle m ON j1.msisdn = m.msisdn AND TO_CHAR(j1.date, 'YYYY-MM') = m.mois
                    where region = 'KARA'
                    
                        union all
                    SELECT DISTINCT ON(j2.msisdn)
                    j2.region as reg1,
                    j2.msisdn as ms1,
                    m.ca AS caa
                FROM
                    info_pdv_journalier j2
                INNER JOIN
                    info_pdv_mensuelle m ON j2.msisdn = m.msisdn AND TO_CHAR(j2.date, 'YYYY-MM') = m.mois
                    where region = 'CENTRALE'
                    
                        union all
                    SELECT DISTINCT ON(j3.msisdn)
                    j3.region as reg1,
                    j3.msisdn as ms1,
                    m.ca AS caa
                FROM
                    info_pdv_journalier j3
                INNER JOIN
                    info_pdv_mensuelle m ON j3.msisdn = m.msisdn AND TO_CHAR(j3.date, 'YYYY-MM') = m.mois
                    where region = 'PLATEAU'
                    
                        union all
                    SELECT DISTINCT ON(j4.msisdn)
                    j4.region as reg1,
                    j4.msisdn as ms1,
                    m.ca AS caa
                FROM
                    info_pdv_journalier j4
                INNER JOIN
                    info_pdv_mensuelle m ON j4.msisdn = m.msisdn AND TO_CHAR(j4.date, 'YYYY-MM') = m.mois
                    where region = 'SAVANE'
                ORDER BY
                reg1 , ms1, caa DESC ;
        `;

            await client.query(createTableQuery);
            logger.info('La table "toppdv" a été populée avec succès.');
        } else {
            logger.error('La table "toppdv" n\'existe pas.');
        }

        client.release();
    } catch (error) {
        logger.error('Erreur lors de linsertion de la table toppdv :', error);
        throw error;
    }
}

module.exports = toppdv;