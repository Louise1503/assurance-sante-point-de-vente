const winston = require('winston');
const { createLogger, format, transports } = winston;
const DailyRotateFile = require('winston-daily-rotate-file');

// Function to configure and create the logger
function getConfig(logsPath) {
    return winston.createLogger({
        transports: [
            new winston.transports.DailyRotateFile({
                filename: logsPath + '%DATE%.log',
                datePattern: 'YYYY-MM-DD',
                zippedArchive: false,
                maxSize: '450m',
                maxFiles: '30d',
                level: 'info',
                format: winston.format.combine(
                    winston.format.printf((info) => {
                        return `${(new Date()).toJSON().replace("T", " ").replace("Z", "")}; ${info.level.toUpperCase()}; ${info.message}`;
                    })
                )
            }),
            new (winston.transports.Console)({
                level: 'info',
                format: winston.format.combine(
                    winston.format.colorize(),
                    winston.format.printf((info) => {
                        return `${(new Date()).toJSON().replace("T", " ").replace("Z", "")}; ${info.level}; ${info.message}`;
                    })
                )
            })
        ]
    });
}
module.exports = { getConfig};