const dotenv = require('dotenv');
dotenv.config({ path: '../.env' });

const dbConnection = {
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    schema: process.env.DB_SCHEMA
};

module.exports = { dbConnection };
