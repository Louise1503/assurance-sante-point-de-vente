const axios = require('axios');
const cron = require('node-cron');
const { getConfig } = require('../logs/appLog');
const Duration = require('duration-timer');
const { Client, Pool } = require("pg");
const { dbConnection } = require('../models/database');
const dotenv = require('dotenv');
const control = require('../services/control');
dotenv.config({ path: '../.env' });

const logsPath = '../logs/';

const logger = getConfig(logsPath);

// Fonction pour récupérer les numéros de téléphone depuis la base de données
async function fetchPhoneNumbers() {
    const client = new Client(dbConnection);
    await client.connect();

    try {
        const result = await client.query('SELECT ms1 FROM top500 WHERE ms1 NOT IN ( SELECT ms1 FROM sauvegarde )');
        return result.rows.map(row => row.ms1); // Extract phone numbers from the query result
    } catch (error) {
        console.error('Erreur lors de la récupération des numéros de téléphone :', error.message);
        throw error;
    } finally {
        await client.end();
    }
}

async function sendSMS() {
    const API_URL = process.env.API_URL;
    const headers = {
        'Content-Type': 'application/json',
    };

    try {
        const destinationNumbers = await fetchPhoneNumbers(); // Fetch phone numbers from the database
        const data = {
            appName: process.env.APPNAME,
            senderOfSms: process.env.SENDEROFSMS,
            smsAccount: process.env.SMSACCOUNT,
        };

        for (const phoneNumber of destinationNumbers) {
            data.destination = phoneNumber;
            data.message = process.env.NOUVEAU_BENEFICIAIRE;

            const response = await axios.post(API_URL, data, { headers });
            console.log(`SMS sent to ${phoneNumber} successfully! Provider response:`, response.data);
        }
    } catch (error) {
        //console.error('Error sending SMS:', error.message);
        logger.error('Les erreurs:' + error);
    }
}

// Fonction pour exécuter l'envoi de SMS en utilisant le planning cron récupéré depuis la base de données
let cronTask;
async function sendSMSNewbeneficiary() {
    const client = new Client(dbConnection);
    await client.connect();

    try {
        const taskId = 6; // Remplacez par l'ID de la tâche dont vous souhaitez récupérer le planning cron
        const result = await client.query('SELECT cron FROM tache WHERE id_t = $1', [taskId]);
        const cronExpression = result.rows[0].cron;

        // Planifier la tâche cron pour s'exécuter selon le planning récupéré depuis la base de données
const cronTask = cron.schedule(cronExpression, () => {

            logger.info('Cron job started for task ID:' + taskId);
            const duration = new Duration();
            // Call the function to send the SMS
            sendSMS();

            // Log the end of the cron task
            logger.info('Cron job completed for task ID:' + taskId);
            const state = duration.getState();
            logger.info('l\'envoi d\'sms a duré' + JSON.stringify(state));
        }, {
            scheduled: true,
            timezone: 'Africa/Lome', // Remplacez par le fuseau horaire approprié
        });
    } catch (error) {
        console.error('Erreur lors de la récupération du planning cron depuis la base de données :', error.message);
    } finally {
        await client.end();
    }
}

function arreterCron() {

    cronTask.stop();
    console.log('Cron arrêté.');

}

async function beneficiarySms(){
    try {
        control(sendSMSNewbeneficiary,6,arreterCron)
    } catch (error) {
        console.log('erreur:',error)
    }
}
// Appeler la fonction pour exécuter l'envoi de SMS en utilisant le planning cron récupéré depuis la base de données
module.exports =  beneficiarySms ;
