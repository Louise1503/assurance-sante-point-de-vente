const { Client, Pool } = require("pg");
const dotenv = require('dotenv');
dotenv.config({ path: '../.env' });
const cron = require('node-cron');
dotenv.config({ path: '../.env' });

const dbConnection = {
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    schema: process.env.DB_SCHEMA
}
const pool = new Pool(dbConnection);

async function control(mafonction, taskId, arreterCron){
    const client = await pool.connect();
    try {
        const queryResult = await client.query('SELECT * FROM tache WHERE id_t = $1', [taskId]);

        //console.log('Résultat de la requête :', queryResult); // Afficher le résultat de la requête

        if (queryResult.rows[0].active = true){
            if (queryResult.rows[0].repetion = 'often') { 
                mafonction;
            } else if (queryResult.rows[0].repetion = 'once'){

                const dateDebut = queryResult.rows[0].date_fin;
                const dateFin = queryResult.rows[0].date_debut;
                const dateActuelle = new Date(); 

                if (dateActuelle >= dateDebut && dateActuelle <= dateFin ){
                    mafonction;
                } else {
                    console.log('Nous ne sommes pas dans les délais de la tâche.');
                }
            }

        } else if (queryResult.rows[0].active = false){
            arreterCron;
        }

        
    } catch (error) {
        console.error('Erreur lors de l\'exécution de la requête :', error);
        
    } finally {
        client.release();
    }

}
module.exports = control;