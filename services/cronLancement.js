const axios = require('axios');
const cron = require('node-cron');
const { getConfig } = require('../logs/appLog');
const Duration = require('duration-timer');
const { Client, Pool } = require("pg");
const { dbConnection } = require('../models/database')
const dotenv = require('dotenv');
dotenv.config({ path: '../.env' });
const control = require('./control');
const logsPath = '../logs/';
const logger = getConfig(logsPath);
const pool = new Pool(dbConnection);

// Function to retrieve phone numbers from the database
async function fetchPhoneNumbers() {
    const client = new Client(dbConnection);
    await client.connect();

    try {
        const result = await client.query('SELECT ms1 FROM toppdv');
        const phoneNumbers = result.rows.map(row => row.ms1); // Extract phone numbers from the query result
        logger.info(`Retrieved ${phoneNumbers.length} phone numbers from the database.`);
        return phoneNumbers;

    } catch (error) {
        console.error('Error fetching phone numbers:', error.message);
        throw error;
    } finally {
        await client.end();
    }
}



async function sendSMS(from, to, text, dlrurl = "", token) {
    text = encodeURIComponent(text);
    const url = `http://www.wassasms.com/wassasms/api/web/v3/sends?access-token=${token}&sender=${from}&receiver=${to}&text=${text}&dlr_url=${dlrurl}`;

    try {
        const response = await axios.get(url);
        const responseData = response.data;
        return responseData;
    } catch (error) {
        throw error;
    }
}

async function sendBulkSMS() {
    const API_KEY = 'Xt3rB1mmsrV-V3t5QXaTPc1lxCKuu-VX';
    const destinationNumbers = await fetchPhoneNumbers(); // Fetch phone numbers from the database

    const from = process.env.SENDEROFSMS;
    const text = process.env.LANCEMENT;
    const token = API_KEY;

    for (const phoneNumber of destinationNumbers) {
        try {
            await sendSMS(from, phoneNumber, text, "", token);
            logger.info(`SMS sent to ${phoneNumber} successfully!`);
        } catch (error) {
            logger.error(`Error sending SMS to ${phoneNumber}:`, error.message);
        }
    }
}




// Function to execute sending SMS using the cron schedule retrieved from the database
let cronTask;
async function sendSMSLancement() {
    const client = new Client(dbConnection);
    await client.connect();

    try {
        const taskId = 8; 
        const result = await client.query('SELECT cron FROM tache WHERE id_t = $1', [taskId]);
        const cronExpression = result.rows[0].cron;

        const cronTask = cron.schedule(cronExpression, () => {
            //logger.info('Cron job started for task ID:' + taskId );
            sendBulkSMS();
            //logger.info('Cron job completed for task ID:'+ taskId );
//
    }, {
            scheduled: true,
            timezone: 'Africa/Lome', // Replace with the appropriate timezone
        });
    } catch (error) {
        console.error('Error retrieving cron schedule from the database:', error.message);
        
    } finally {
        await client.end();
    }
}


// function measureCronDuration(callback) {
//     const startTime = Date.now();
//     sendSMSLancement();

//     setTimeout(() => {
//         const endTime = Date.now();
//         const duration = endTime - startTime;
//         callback(duration);
//     }, 5500); // Wait a little longer to ensure the task is complete
// }


// measureCronDuration((duration) => {
//     console.log(`Cron task took ${duration}ms to complete.`);
// });

//sendSMSLancement();
function arreterCron() {
    if (cronTask) {
        cronTask.stop();
        console.log('Cron arrêté.');
    }
    
}
async function LancementSms(){
    try {
        control(sendSMSLancement(), 8, arreterCron())
    } catch (error) {
        logger.error('erreurs:',error);
    }
}

//module.exports =  LancementSms ;
LancementSms();
