SELECT DISTINCT ON(j.msisdn)
    j.msisdn,
	j.region,
	TO_CHAR(j.date, 'YYYY-MM') as moisj,
	SUM(m.sell_out_mois) AS caa
    --m.ca AS caa
FROM
    info_pdv_journalier j
INNER JOIN
    info_pdv_mensuelle m ON j.msisdn = m.msisdn AND TO_CHAR(j.date, 'YYYY-MM') = m.mois1
	GROUP BY
        m.mois1, m.msisdn , j.region, j.msisdn,  j.date
ORDER BY
    j.msisdn,j.region,moisj, caa DESC LIMIT 500;



    WITH info_pdv_mensuelle AS (
    SELECT DISTINCT ON(msisdn)
        msisdn,
		mois1,
        SUM(sell_out_mois) AS ca
    FROM
        info_pdv_mensuelle
    WHERE
        date >= DATE_TRUNC('year', NOW()) - INTERVAL '1 year' 
    GROUP BY
        msisdn , mois1
)

SELECT DISTINCT ON(j.msisdn)
    j.msisdn,
	j.region,
	TO_CHAR(j.date, 'YYYY-MM') as moisj,
    m.ca AS caa
FROM
    info_pdv_journalier j
    INNER JOIN
    info_pdv_mensuelle m ON j.msisdn = m.msisdn AND TO_CHAR(j.date, 'YYYY-MM') = m.mois1
ORDER BY
    j.msisdn,j.region,moisj, m.ca DESC LIMIT 500;