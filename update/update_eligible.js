const cron = require('node-cron');
const { dbConnection } = require('../models/database');
const  eligible  = require('../controllers/eligible');
const dotenv = require('dotenv');
dotenv.config({ path: '../.env' });
const { Client } = require('pg');
const logsPath = '../logs/';
const { getConfig } = require('../logs/appLog');
const logger = getConfig(logsPath);


async function update_eligible() {
    cron.schedule('0 0 1 */6 *', async () => {
        const client = new Client(dbConnection);
        await client.connect();
        try {
            const countQuery = 'SELECT COUNT(*) AS count FROM eligible';
            const countResult = await client.query(countQuery);
            const rowCount = countResult.rows[0].count;
            if (rowCount > 0) {
                const deleteQuery = 'DELETE FROM eligible';
                await client.query(deleteQuery);
                console.log(`Supprimé ${rowCount} éléments de la table "eligible".`);
            } else {
                console.log('Aucun élément à supprimer dans la table "eligible".');
            }
            logger.info('Mise à jour de la table « eligible »...');
            await eligible();
            logger.info('La table "eligible" a été actualisée.');
        } catch (error) {
            logger.error('Erreur lors de l\'actualisation de la table eligible :', error);
        } finally {
            await client.end();
        }
    });
}


module.exports = update_eligible;