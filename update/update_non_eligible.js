const cron = require('node-cron');
const pool = require('../models/database');
const { non_eligible } = require('../controllers/non_eligible');
const dotenv = require('dotenv');
dotenv.config({ path: '../.env' });
const { Client } = require('pg');
const logsPath = '../logs/';
const { getConfig } = require('../logs/appLog');
const logger = getConfig(logsPath);


// Planifiez le cron job pour qu'il s'exécute tous les 6 mois
async function update_non_eligible(){
    cron.schedule('0 0 1 */6 *', async () => {
    const client = new Client(dbConnection);
    await client.connect();
    try {
        const countQuery = 'SELECT COUNT(*) AS count FROM non_eligible';
        const countResult = await client.query(countQuery);
        const rowCount = countResult.rows[0].count;
        if (rowCount > 0) {
            const deleteQuery = 'DELETE FROM sauvegarde';
            await client.query(deleteQuery);
            console.log(`Supprimé ${rowCount} éléments de la table "non_eligible".`);
        } else {
            console.log('Aucun élément à supprimer dans la table "non_eligible".');
        }
        console.log('Mise à jour de la table « non_eligible »...');
        await non_eligible();
        console.log('La table "non_eligible" a été actualisée.');
    } catch (error) {
        console.error('Erreur lors de l\'actualisation de la table non_eligible :', error);
    } finally {
        await client.end(); 
    }
});
}


module.exports = update_non_eligible;




