const cron = require('node-cron');
const { dbConnection } = require('../models/database');
const dotenv = require('dotenv');
const pool = require('../models/database')
dotenv.config({ path: '../.env' });
const { Client } = require('pg');
const logsPath = '../logs/';
const { getConfig } = require('../logs/appLog');
const logger = getConfig(logsPath);

async function update_sauvegarde() {
    async function sauvegardeEligible() {
        try {
            const client = await pool.connect();
            
    const insertQuery = `
            INSERT INTO sauvegarde 
            SELECT reg1, ms1, caa
            FROM eligible;
        `;
            await client.query(insertQuery);
            console.log('Les données de la vue  "vue_eligible"ont été inseré avec succes dans la table sauvegarde.');
                client.release();
} catch (error) {
    console.error('Erreur lors de la sauvegarde de la vue :', error);
    throw error;
}
}

//la mise a jour de la table sauvegarde se fait apres chaque  mois a minuit
cron.schedule('* * * * *', async () => {
    const client = new Client(dbConnection);
    await client.connect();
    try {
        const countQuery = 'SELECT COUNT(*) AS count FROM sauvegarde';
        const countResult = await client.query(countQuery);
        const rowCount = countResult.rows[0].count;
        if (rowCount > 0) {
            const deleteQuery = 'DELETE FROM sauvegarde';
            await client.query(deleteQuery);
            console.log(`Supprimé ${rowCount} éléments de la table "sauvegarde".`);
        } else {
            console.log('Aucun élément à supprimer dans la table "sauvegarde".');
        }
        console.log('Actualisation de la table"sauvegardeEligible" ');
        await sauvegardeEligible();
        console.log('La table "sauvegardeEligible" a été actualisée.');
    } catch (error) {
        console.error('Erreur lors de l\'actualisation de la table sauvegarde :', error);
    } finally {
        await client.end();
    }
});
}
update_sauvegarde();
module.exports = { update_sauvegarde };