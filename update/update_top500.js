const cron = require('node-cron');
const { top500 } = require('../controllers/top500'); 
const { dbConnection } = require('../models/database');
const dotenv = require('dotenv');
dotenv.config({ path: '../.env' });
const logsPath = '../logs/';
const { getConfig } = require('../logs/appLog');
const logger = getConfig(logsPath);


//Le cron tourne chaque premier du mois a 4h du matin
async function update_top500() 
{
    const client = new Client(dbConnection);
    await client.connect();
    cron.schedule('0 4 1 * *', async () => {
    try {
        const countQuery = 'SELECT COUNT(*) AS count FROM top500';
        const countResult = await client.query(countQuery);
        const rowCount = countResult.rows[0].count; 
        if (rowCount > 0) {
            const deleteQuery = 'DELETE FROM top500';
            await client.query(deleteQuery);
            console.log(`Supprimé ${rowCount} éléments de la table "top500".`);
        } else {
            console.log('Aucun élément à supprimer dans la table "top500".');
        }
        console.log('Mise à jour de la table « top500 »...');
        await top500(); 
        console.log('La table « top500 » a été mise à jour.');
    } catch (error) {
        console.log('erreur lors de la mise ajour de la table top500:', error);
    } finally {
        await client.end(); 
    }
    }
);
}

module.exports = update_top500;