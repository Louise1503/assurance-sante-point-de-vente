const cron = require('node-cron');
const { Client } = require('pg');
const toppdv = require('../controllers/toppdv');
const { dbConnection } = require('../models/database');
const dotenv = require('dotenv');
dotenv.config({ path: '../.env' });
const logsPath = '../logs/';
const { getConfig } = require('../logs/appLog');
const logger = getConfig(logsPath);

// le cron tourne le 1er chaque mois à 2h du matin
async function update_toppdv() {
    cron.schedule('* * * * *', async () => { 
        const client = new Client(dbConnection);
        await client.connect();
        try {
            const countQuery = 'SELECT COUNT(*) AS count FROM toppdv';
            const countResult = await client.query(countQuery); 
            const rowCount = countResult.rows[0].count; 
            
            if (rowCount > 0) {
                const deleteQuery = 'DELETE FROM toppdv';
                await client.query(deleteQuery);
                console.log(`Supprimé ${rowCount} éléments de la table "toppdv".`);
            } else {
                console.log('Aucun élément à supprimer dans la table "toppdv".');
            }

            console.log('Mise à jour de la table « toppdv »...');
            await toppdv(); // Attendre que la fonction toppdv() se termine
            console.log('La table « toppdv » a été mise à jour.');
        } catch (error) {
            console.log('erreur lors de la mise a jour de la table toppdv :', error);
        } finally {
            await client.end(); // N'oubliez pas de fermer la connexion client
        }
    });
}

update_toppdv();

//module.exports = update_toppdv;